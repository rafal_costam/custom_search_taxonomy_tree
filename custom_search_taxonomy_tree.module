<?php

/**
 * @file
 * Bring customizations to the default search box
 *
 * Adds taxonomy options in tree form to the search form
 */

function popz_taxonomy_get_nested_tree($vid_or_terms = array(), $max_depth = NULL, $parent = 0, $parents_index = array(), $depth = 0) {

  if (!is_array($vid_or_terms)) {
    $vid_or_terms = taxonomy_get_tree($vid_or_terms);
  }

  foreach ($vid_or_terms as $term) {

    foreach ($term->parents as $term_parent) {
      if ($term_parent == $parent) {
        $return[$term->tid] = $term;
      }
      else {
        $parents_index[$term_parent][$term->tid] = $term;
      }
    }
  }

  foreach ($return as &$term) {
    if (isset($parents_index[$term->tid]) && (is_null($max_depth) || $depth < $max_depth)) {
      $term->children = popz_taxonomy_get_nested_tree($parents_index[$term->tid], $max_depth, $term->tid, $parents_index, $depth + 1);
    }
  }

  return $return;
}

function popz_taxonomy_nested_tree_theme_render($tree, $recurring = FALSE) {
    $items = '';
    $vid = -1;

    if (count($tree)) {
        foreach ($tree as $term) {
            if ($vid < 0) {
                $vid = $term->vid;
            }
            $value = 'c-' . $term->tid;
            $id = sprintf('edit-custom-search-vocabulary-%d-%s', $term->vid, $value);
            $name = sprintf('custom_search_vocabulary_%d[%s]', $term->vid, $value);
            $element = array(
                '#type' => 'checkbox',
                '#title' => $term->name,
                '#return_value' => $value,
                '#id' => $id,
                '#attributes' => array(
                    'name' => $name,
                    'id' => $id
                )
            );
            $current = theme("checkbox", array('element' => $element));
            $current .= theme("form_element_label", array('element' => $element));
            $current = sprintf('<div class="form-type-checkbox form-item checkbox">%s</div>', $current);

            if (isset($term->children)) {
                $container = sprintf('<div class="form-wrapper form-group">%s</div>', popz_taxonomy_nested_tree_theme_render($term->children, TRUE));
                $items .= sprintf('<div class="parent-form-item form-wrapper form-group">%s%s</div>', $current, $container);
            }
            else {
                $items .= $current;
            }
        }
    }

    if ($recurring) {
        return $items;
    }
    return sprintf('<div id="edit-custom-search-vocabulary-%d">%s</div>', $vid, $items);
}

function custom_search_taxonomy_tree_theme() {
    $themes = array (
        'taxonomy_search_tree_checkboxes_container' => array(
            'render element' => 'element',
            'file' => 'custom_search_taxonomy_tree.theme.inc',
        ),
    );
    return $themes;
}

function custom_search_taxonomy_tree_form_search_block_form_alter(&$form, &$form_state, $form_id) {
    $form['custom_search_vocabulary_1']['#theme_wrappers'] = array('taxonomy_search_tree_checkboxes_container');
}